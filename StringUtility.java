
public class StringUtility {

	public int add(String word) {
		int result = 0;
		char[] delimiters = {'/',',', '\n', ';', '*', '[', ']', '%'};
		boolean findDelimiters = false;
		StringBuilder newWord = new StringBuilder();
		if (!word.equals("")){
			
			for (int i = 0 ; i < word.length(); i++){
				for(int j = 0; j < delimiters.length; j++){
					findDelimiters = isOccurredDelimiter(word, delimiters, findDelimiters, newWord, i, j);
				}
				if (!findDelimiters){
				newWord.append(word.charAt(i));
				}
				findDelimiters = false;
			}
			
			String[] numbers = newWord.toString().split(";");
		
			for(String number : numbers){
				result = checkAndAddNumber(result, number);
			}
		}
		return result;
		
	}

	/**
	 * @param result
	 * @param number
	 * @return
	 */
	private int checkAndAddNumber(int result, String number) {
		if (!number.equals("")){
			Integer resNumber = Integer.parseInt(number);
			if(resNumber <= 1000){
				result += resNumber;
			};
		}
		return result;
	}

	/**
	 * @param word
	 * @param delimiters
	 * @param findDelimiters
	 * @param newWord
	 * @param i
	 * @param j
	 * @return
	 */
	private boolean isOccurredDelimiter(String word, char[] delimiters, boolean findDelimiters, StringBuilder newWord,
			int i, int j) {
		if (word.charAt(i) == delimiters[j]){
			newWord.append(';');
			findDelimiters = true;
		}
		return findDelimiters;
	}

}
